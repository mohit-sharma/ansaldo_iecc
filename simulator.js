var express = require('express');
var dbOperations = require('./dbOperations');
var utils = require('./utils');
var app = express();
var router = express.Router();
var bodyParser = require("body-parser");
var snmp = require('./cronJob.js');
var js2xmlparser = require("js2xmlparser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/public/views');
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
var alreadyCreatedNodes = [];

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    // here We can do validations to make sure that everything coming from a request is safe and sound
    //console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

app.get('/', function (req, res) {
  var Networkinterfaces = require('os').networkInterfaces();
  var NetwoekInterfaceList = Object.keys(Networkinterfaces);
  NetwoekInterfaceList.splice(NetwoekInterfaceList.indexOf('lo'), 1);
  var ip = req.headers.host.split(":")[0];
  res.render('ipConfiguration.ejs', {
    NetwoekInterfaceList : NetwoekInterfaceList,
    alreadyCreatedNodes : alreadyCreatedNodes
  });
})

app.get('/node', function (req, res) {
  //res.send('Welcome to IECC Simulator.'+ require('os').networkInterfaces());
  var ip = req.headers.host.split(":")[0];
  dbOperations.getStaticData(ip, function(err, data){
    if(err)
      res.send(err);
    res.render('node.ejs', {
      nodeInfo : data,
      nodeIP : ip + ":8081"
    });
  })
})

app.post('/createIP', function(req, res, next) {
    if (!req.body) return res.send(400)
    global.numberOfIP = req.body.numberOfIp;
    utils.BindStaticIP(req.body.numberOfIp, req.body.networkInterface, function(createdIPList){
      alreadyCreatedNodes = createdIPList;
      return res.send({"status" : "success", "IpList" : createdIPList});
    });
});

// Creating APIs
router.route('/getStaticInfo')
    .get(function(req, res) {
      var ip = req.headers.host.split(":")[0];
      dbOperations.getStaticData(ip, function(err, data){
        if(err)
          res.send(err);
        res.json({ status : 'OK', Data: data });
      })
    });

router.route('/getStaticInfoXML')
    .get(function(req, res) {
      var ip = req.headers.host.split(":")[0];
      dbOperations.getStaticData(ip, function(err, data){
        if(err)
          res.send(err);
        res.set('Content-Type', 'text/xml');
        res.send(js2xmlparser.parse("LongTermData", data));
        //res.json({ status : 'OK', Data: data });
      })
    });

    // Creating APIs
router.route('/getDynamicInfo/:from_time_stamp')
        .get(function(req, res) {
          var ip = req.headers.host.split(":")[0];
          dbOperations.getDynamicData(ip, req.params.from_time_stamp, function(err, data){
            if(err)
              res.send(err);
            res.json({ status : 'OK', Data: data });
          })
        });

app.use('/api', router);

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port

   console.log("Simulator listening at port : %s", port)
})

var fs = require('fs');
var path = require('path');
router.route('/genMotorCurrentJson').get(function(req, res) {
   	var table= [];	
	 var ip = req.headers.host.split(":")[0];
	var d = new Date();
    var n = d.getMinutes();
	var time = -1.0;
	var threw = 0.74;
	var j,k;
	if (n % 2 === 0) {
		j=0.023001;k=0.000041;
	} else{
		j=0.024001;k=0.000042;
	}
	//for (i=0; i<2 ; i++){    	table.push({time: ((-1.000001).toFixed(6))/1, throw: (0.750000.toFixed(6))/1});    }
	for (i=0; i<250 ; i++){        table.push({"time": time.toFixed(6)/1, "throw": threw.toFixed(6)/1});	time = time+j; threw= threw+k;      }
      //  var json = JSON.stringify(table);
	var fileName=d.getFullYear()+'-'+d.getMonth()+'-'+d.getDate()+'_'+d.getHours()+'-'+d.getMinutes()+'-'+d.getSeconds()+'.json';
     //   fs.writeFile('./repo/motorCurrentFiles/'+fileName, json);
        res.json({ status : 'OK', Data: table, fileName : fileName });
     // dbOperations.getStaticData(ip, function(err, data){        if(err)          res.send(err);
       // res.set('Content-Type', 'text/xml');
      //  res.send(js2xmlparser.parse("LongTermData", data));
        //res.json({ status : 'OK', Data: data });
     // })
	
    });

_ = require('underscore');
router.route('/getMotorCurrentFile').get(function(req, res) {
	var ip = req.headers.host.split(":")[0];
	 dbOperations.getIdAndLatestCurrentFileName(ip, function(err, data){
		 if(err)
			 console.log('motor file db select err');
		 else {
			 var dirPath = path.join(__dirname +'/db/'+data[0].iecc_node_id);
    var files = fs.readdirSync(dirPath);
    var latestFile = _.max(files, function (files) {
        var fullpath = path.join(dirPath, files);
        return fs.statSync(fullpath).ctime;
    });	 
	  var filePath = path.join(__dirname +'/db/'+data[0].iecc_node_id+'/', latestFile);
    var stat = fs.statSync(filePath);
	var readStream = fs.createReadStream(filePath);
	res.writeHead(200, {"file":latestFile,
       "Content-Type": "application/octet-stream",
          "Content-Disposition" : "attachment; filename=" + latestFile});
		readStream.pipe(res);
		 }
	 });
});

/* router.route('/getEventLogs').get(function(req, res) {
	var ip = req.headers.host.split(":")[0];
	 dbOperations.getIdAndLatestCurrentFileName(ip, function(err, data){
		 if(err)
			 console.log('event file db select err');
		 else {
	  var filePath = path.join(__dirname +'/db/'+data[0].iecc_node_id, 'events.db');
    var stat = fs.statSync(filePath);
	var readStream = fs.createReadStream(filePath);
    
	 res.writeHead(200, {
          "Content-Type": "application/octet-stream",
          "Content-Disposition" : "attachment; filename=" + 'events.db'});
		readStream.pipe(res);
			 }
	 });
}); */

router.route('/getEventLogs/:id/:recordsLimit').get(function(req, res) {
          var ip = req.headers.host.split(":")[0];
		 dbOperations.getIdAndLatestCurrentFileName(ip, function(err, data){
		 if(err)
			 console.log('maintenance file db select err');
		 else {
		 var dbPath = path.join(__dirname +'/db/'+data[0].iecc_node_id, 'events.db');
			// console.log('filePath='+dbPath);
			 dbOperations.getLatestEventLog(dbPath, req.params.id, req.params.recordsLimit, function(err, data){
            if(err){
             	console.log('err = '+err);
				res.send(err);
			}
			if(data)
			{
			//	console.log(JSON.stringify(data));
              res.send(data);
			}
          });
	}
	});
 });

router.route('/getSwopLogs/:id/:recordsLimit').get(function(req, res) {
          var ip = req.headers.host.split(":")[0];
		 dbOperations.getIdAndLatestCurrentFileName(ip, function(err, data){
		 if(err)
			 console.log('maintenance file db select err');
		 else {
		 var dbPath = path.join(__dirname +'/db/'+data[0].iecc_node_id, 'swop.db');
			// console.log('filePath='+dbPath);
			 dbOperations.getLatestSwopLog(dbPath, req.params.id, req.params.recordsLimit, function(err, data){
            if(err){
             	console.log('err = '+err);
				res.send(err);
			}
			if(data)
			{
			//	console.log(JSON.stringify(data));
              res.send(data);
			}
          });
	}
	});
 });

router.route('/getMaintenanceLogs/:id/:recordsLimit').get(function(req, res) {
          var ip = req.headers.host.split(":")[0];
		 dbOperations.getIdAndLatestCurrentFileName(ip, function(err, data){
		 if(err)
			 console.log('maintenance file db select err');
		 else {
		 var dbPath = path.join(__dirname +'/db/'+data[0].iecc_node_id, 'maintenance.db');
			// console.log('filePath='+dbPath);
			 dbOperations.getLatestMaintenanceLog(dbPath, req.params.id, req.params.recordsLimit, function(err, data){
            if(err){
             	console.log('err = '+err);
				res.send(err);
			}
			if(data)
			{
			//	console.log(JSON.stringify(data));
              res.send(data);
			}
          });
	}
	});
 });

/* router.route('/getMaintenanceLogs').get(function(req, res) {
		var ip = req.headers.host.split(":")[0];
	 dbOperations.getIdAndLatestCurrentFileName(ip, function(err, data){
		 if(err)
			 console.log('maintenance file db select err');
		 else {
		 var filePath = path.join(__dirname +'/db/'+data[0].iecc_node_id, 'maintenance.db');
    var stat = fs.statSync(filePath);
	var readStream = fs.createReadStream(filePath);
	 res.writeHead(200, {
          "Content-Type": "application/octet-stream",
          "Content-Disposition" : "attachment; filename=" + 'maintenance.db'});
		readStream.pipe(res);
			 			 }
	 });
    }); 

router.route('/getSwopLogs').get(function(req, res) {
			var ip = req.headers.host.split(":")[0];
	 dbOperations.getIdAndLatestCurrentFileName(ip, function(err, data){
		 if(err)
			 console.log('swop file db select err');
		 else {
		 var filePath = path.join(__dirname +'/db/'+data[0].iecc_node_id, 'swop.db');
    var stat = fs.statSync(filePath);
	var readStream = fs.createReadStream(filePath);
	 res.writeHead(200, {
          "Content-Type": "application/octet-stream",
          "Content-Disposition" : "attachment; filename=" + 'swop.db'});
		readStream.pipe(res);
			 }
	 });
    });
	*/

var io = require('socket.io')(server);
io.on('connection', function (socket) {
    console.log('hell boss is connected!');
    socket.emit('message', 'You are connected!');
	socket.on('getInstData', function(message){
		dbOperations.getStaticData(message, function(err, data){
        if(err)
          console.log(' db err ');
		else
		socket.emit('getInstData', js2xmlparser.parse("LongTermData", data));
      });	
  }); 
	
	socket.on('getMaintData', function(ip,id){
	//var ip = req.headers.host.split(":")[0];
		
		console.log('ip='+ip);
		 dbOperations.getIdAndLatestCurrentFileName(ip, function(err, data){
		 if(err)
			 console.log('maintenance file db select err');
		 else {
		 var dbPath = path.join(__dirname +'/db/'+data[0].iecc_node_id, 'maintenance.db');
			// console.log('filePath='+dbPath);
			 dbOperations.getLatestMaintenanceLog(dbPath, id, function(err, data){
            if(err){
             	console.log('err = '+err);
				res.send(err);
			}
			if(data)
			{
				console.log(JSON.stringify(data));
             // res.send(data);
				socket.emit('sendMaintData', data);
			}
          });
	}
	});
	});
	
});