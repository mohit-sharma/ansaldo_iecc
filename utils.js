//var sys = require('sys')
var exec = require('child_process').exec;
var fs = require('fs');
var path = require('path');
var ipListPath = path.join(__dirname, '.', 'ipList.json');
var objIP = JSON.parse(fs.readFileSync(ipListPath, 'utf8'));
var utils = {};
var os = require('os');
var snmpj = require('snmpjs');
var dbOperations = require('./dbOperations');
var copydir = require('copy-dir');
var highestCreatedNode = 0;

utils.BindStaticIP = function(numberOfIP, networkInterface, cb){
  var createdIPList = [];
  utils.RemovePerviousIPs(numberOfIP, networkInterface, function(numberOfIP, networkInterface){
    //if(highestCreatedNode != 0)
      //highestCreatedNode = numberOfIP > highestCreatedNode ? numberOfIP : highestCreatedNode;
    var command = "";
    console.log(numberOfIP);
    for(var i = 0; i < numberOfIP; i++ ){
      console.log(objIP[i].ip);
      console.log(numberOfIP + '|' + i); 
      createdIPList.push(objIP[i].ip);
      command = "sudo ip address add "+ objIP[i].ip +"/24 dev " + networkInterface;
      //console.log(command);
      var counter=0;
      exec(command, function (error, stdout, stderr) {

        //if(counter > highestCreatedNode){
          console.log('H :' + highestCreatedNode + " Counter : "+ counter);
          var agent = snmpj.createAgent();
          //var agent2 = snmpj.createAgent();

          agent.request({ oid: '.1.3.6.1.2.1.1.1', handler: function (prq) {
            dbOperations.GetSpecificStatus('health_status', objIP[counter].ip, function(row){
              var val = snmpj.data.createData({ type: 'OctetString', value: JSON.stringify(row[0]) });
              snmpj.provider.readOnlyScalar(prq, val);
            });
          }
        });

          agent.request({ oid: '.1.3.6.1.2.1.1.2', handler: function (prq) {
            dbOperations.GetSpecificStatus('latch_out_status', objIP[counter].ip, function(row){
              var val = snmpj.data.createData({ type: 'OctetString', value: JSON.stringify(row[0]) });
              snmpj.provider.readOnlyScalar(prq, val);
            });
          }
        });

          agent.request({ oid: '.1.3.6.1.2.1.1.3', handler: function (prq) {
            dbOperations.GetSpecificStatus('aux_sensors_value', objIP[counter].ip, function(row){
              var val = snmpj.data.createData({ type: 'OctetString', value: JSON.stringify(row[0]) });
              snmpj.provider.readOnlyScalar(prq, val);
            });
          }
        });

          agent.request({ oid: '.1.3.6.1.2.1.1.4', handler: function (prq) {
            dbOperations.GetSpecificStatus('aux_sensors_threshold', objIP[counter].ip, function(row){
              var val = snmpj.data.createData({ type: 'OctetString', value: JSON.stringify(row[0]) });
              snmpj.provider.readOnlyScalar(prq, val);
            });
          }
        });

          agent.request({ oid: '.1.3.6.1.2.1.1.5', handler: function (prq) {
            dbOperations.GetSpecificStatus('motor_current_value', objIP[counter].ip, function(row){
              var val = snmpj.data.createData({ type: 'OctetString', value: JSON.stringify(row[0]) });
              snmpj.provider.readOnlyScalar(prq, val);
            });
          }
        });

          agent.request({ oid: '.1.3.6.1.2.1.1.6', handler: function (prq) {
            dbOperations.GetSpecificStatus('motor_current_threshold', objIP[counter].ip, function(row){
              var val = snmpj.data.createData({ type: 'OctetString', value: JSON.stringify(row[0]) });
              snmpj.provider.readOnlyScalar(prq, val);
            });
          }
        });

          agent.request({ oid: '.1.3.6.1.2.1.1.7', handler: function (prq) {
            dbOperations.GetSpecificStatus('motor_throw_time_value', objIP[counter].ip, function(row){
              var val = snmpj.data.createData({ type: 'OctetString', value: JSON.stringify(row[0]) });
              snmpj.provider.readOnlyScalar(prq, val);
            });
          }
        });

          agent.request({ oid: '.1.3.6.1.2.1.1.8', handler: function (prq) {
            dbOperations.GetSpecificStatus('motor_throw_time_threshold', objIP[counter].ip, function(row){
              var val = snmpj.data.createData({ type: 'OctetString', value: JSON.stringify(row[0]) });
              snmpj.provider.readOnlyScalar(prq, val);
            });
          }
        });

          agent.request({ oid: '.1.3.6.1.2.1.1.9', handler: function (prq) {
            dbOperations.GetSpecificStatus('voltage_value', objIP[counter].ip, function(row){
              var val = snmpj.data.createData({ type: 'OctetString', value: JSON.stringify(row[0]) });
              snmpj.provider.readOnlyScalar(prq, val);
            });
          }
        });

          agent.request({ oid: '.1.3.6.1.2.1.1.10', handler: function (prq) {
            dbOperations.GetSpecificStatus('voltage_red_threshold', objIP[counter].ip, function(row){
              var val = snmpj.data.createData({ type: 'OctetString', value: JSON.stringify(row[0]) });
              snmpj.provider.readOnlyScalar(prq, val);
            });
          }
        });

          agent.request({ oid: '.1.3.6.1.2.1.1.11', handler: function (prq) {
            dbOperations.GetSpecificStatus('voltage_yellow_threshold', objIP[counter].ip, function(row){
              var val = snmpj.data.createData({ type: 'OctetString', value: JSON.stringify(row[0]) });
              snmpj.provider.readOnlyScalar(prq, val);
            });
          }
        });

          agent.bind({ addr: objIP[counter].ip, family: 'udp4', port: 161 })

        //}

        dbOperations.GetSpecificField('iecc_node_id', objIP[counter].ip, function(row){
          var directoryName = 'db/' + row[0]['iecc_node_id'];
          fs.access(path.join(__dirname, directoryName), (err) => {
            if(err) {
              fs.mkdirSync(path.join(__dirname, directoryName))
              var eventLog = 'events.db';
              var maintenanceLog = 'maintenance.db';
              var swopLog = 'swop.db';
              copyFile(path.join(__dirname, 'logs/' + eventLog), path.join(__dirname, directoryName, eventLog));
              copyFile(path.join(__dirname, 'logs/' + maintenanceLog), path.join(__dirname, directoryName, maintenanceLog));
              copyFile(path.join(__dirname, 'logs/' + swopLog), path.join(__dirname, directoryName, swopLog));
              fs.access(path.join(directoryName, 'motorCurrent'), (err) => {
                if(err)
                  fs.mkdirSync(path.join(__dirname, directoryName, 'motorCurrent'));
                copydir.sync(path.join(__dirname, 'logs/motorCurrent'), path.join(__dirname, directoryName, 'motorCurrent'));
              });
            }
          });
        });
        counter ++;
      });
}
  //highestCreatedNode = numberOfIP > highestCreatedNode ? numberOfIP : highestCreatedNode;
})
cb(createdIPList);
}

utils.RemovePerviousIPs = function (numberOfIP, networkInterfac, cb){
  var Networkinterfaces = require('os').networkInterfaces();
  var NetwoekInterfaceList = Object.keys(Networkinterfaces);
  NetwoekInterfaceList.splice(NetwoekInterfaceList.indexOf('lo'), 1);
  // now remove all the IPs in our list from every network interface.
  for (var NI in NetwoekInterfaceList) {
    for(var j = 0; j < 500; j++ ){
      //console.log(objIP[i].ip);
      command = "sudo ip address del "+ objIP[j].ip +"/24 dev " + NetwoekInterfaceList[NI];
      //console.log(command);
      //var counter=0;
      //exec(command, function (error, stdout, stderr) {
      //dbOperations.GetSpecificField('iecc_node_id', objIP[counter].ip, function(row){
        //var directoryName = 'db/' + row[0]['iecc_node_id'];
        //deleteFolderRecursive(directoryName);
      //});

       //counter++;
        //sys.print('stdout: ' + stdout);
        //sys.print('stderr: ' + stderr);
        //if (error !== null) {
          //console.log('exec error: ' + error);
        //}
      //});
    }
  }
  //exec('sudo kill $( sudo lsof -t -i:161 )');
  //exec('sudo fuser -n tcp -k 161');
  
  cb(numberOfIP, networkInterfac);
}

function copyFile(src, dest) {

  var readStream = fs.createReadStream(src);

  readStream.once('error', (err) => {
    console.log(err);
  });

  readStream.once('end', () => {
    //console.log('done copying');
  });

  readStream.pipe(fs.createWriteStream(dest));
}

/*function deleteFolderRecursive(path) {
  if( fs.existsSync(path) ) {
    fs.readdirSync(path).forEach(function(file,index){
      var curPath = path + "/" + file;
      if(fs.lstatSync(curPath).isDirectory()) { // recurse
        deleteFolderRecursive(curPath);
      } else { // delete file
        fs.unlinkSync(curPath);
      }
    });
    fs.rmdirSync(path);
  }
}*/

module.exports = utils;
