var cron = require('node-cron');
var snmp = require('./snmp.js');
var dbOperations = require('./dbOperations.js');

/*
# ┌────────────── second (optional)
# │ ┌──────────── minute
# │ │ ┌────────── hour
# │ │ │ ┌──────── day of month
# │ │ │ │ ┌────── month
# │ │ │ │ │ ┌──── day of week
# │ │ │ │ │ │
# │ │ │ │ │ │
# * * * * * *
*/

var voltageValueList = [250,290,310];

var SNMPCron = cron.schedule('*/10 * * * * *', function(){
  snmp.sendSnmpTraps({"ip":"172.18.42.4","voltage_value": voltageValueList[Math.floor(Math.random() * voltageValueList.length)] });
}, false);

var DynamicDataEntryCron = cron.schedule('* * * * *', function(){
  dbOperations.BulkInsertDynamicData();
}, false);

//SNMPCron.start();
DynamicDataEntryCron.start();
